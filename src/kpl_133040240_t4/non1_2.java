package kpl_133040240_t4;


import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dicki
 */
public class non1_2 {
    public static void main(String []args) {
    // String s may be user controllable
// \uFE64 is normalized to < and \uFE65 is normalized to > using the NFKC
normalization form
String s = "\uFE64" + "script" + "\uFE65";
// Validate
Pattern pattern = Pattern.compile("[<>]"); // Check for angle brackets
Matcher matcher = pattern.matcher(s);
if (matcher.find()) {
// Found black listed tag
throw new IllegalStateException();
} else {
// ...
}
// Normalize
s = Normalizer.normalize(s, Form.NFKC);
}
}